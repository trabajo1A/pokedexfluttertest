import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/route_manager.dart';
import 'package:pokedex_flutter_app/pages/pokemon_favoritos_page.dart';
import 'package:pokedex_flutter_app/pages/pokemon_home_page.dart';
import 'package:pokedex_flutter_app/pages/splashscreen.dart';
import 'package:simple_connection_checker/simple_connection_checker.dart';

String routeGlobal = "";
bool? connected1;

StreamSubscription? subscription;
void main() {
  HttpOverrides.global = new MyHttpOverrides();
  runApp(MyHomePage());
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String? route;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SimpleConnectionChecker _simpleConnectionChecker =
        SimpleConnectionChecker();
    subscription =
        _simpleConnectionChecker.onConnectionChange.listen((connected) {
      setState(() {
        print("=====> Esta conectado: ${connected}");
        if (connected == true) {
          setState(() {
            print("llego a true");
            route = '/home';
            routeGlobal = '/home';
          });
        } else {
          setState(() {
            print("llego a false");
            route = '/fav';
            routeGlobal = '/fav';
          });
        }
        connected1 = connected;
        // if (connected == true) {
        //   if (seDesactivoUnaVez == true) {
        //     seDesactivoUnaVez = false;
        //     Restart.restartApp();
        //   }
        // }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      locale: Locale("es", "ES"),
      // ignore: prefer_const_literals_to_create_immutables
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      // ignore: prefer_const_literals_to_create_immutables
      supportedLocales: [
        const Locale('es', 'ES'),
        const Locale('en', 'US'),
      ],
      debugShowCheckedModeBanner: false,
      title: 'Komunidad',
      theme: ThemeData(
        fontFamily: 'Poppins',
      ),
      getPages: [
        GetPage(
            name: "/",
            page: () => SplashScreen(
                  route: route,
                )),
        GetPage(name: '/home', page: () => HomePagePrincipal()),
        GetPage(name: '/fav', page: () => PokemonFavoritosPage()),
      ],
    );
  }
}
