import 'dart:convert';

class MovementsModel {
  String? name;

  MovementsModel({this.name});

  factory MovementsModel.fromJson(Map<String, dynamic> json) =>
      MovementsModel(name: json["move"]["name"]);

  Map<String, dynamic> toJson() => {"move" "name": name};
}

List<MovementsModel> movementsFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<MovementsModel>.from(str.map((item) {
    return MovementsModel.fromJson(item);
  }));
}

String movementsToJson(MovementsModel movementsModel) {
  final dyn = movementsModel.toJson();
  return json.encode(dyn);
}
