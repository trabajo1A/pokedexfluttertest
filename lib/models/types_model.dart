import 'dart:convert';

class TypesModel {
  String? name;

  TypesModel({this.name});

  factory TypesModel.fromJson(Map<String, dynamic> json) =>
      TypesModel(name: json["types"]["type"]["name"]);

  Map<String, dynamic> toJson() => {"types" "type" "name": name};
}

List<TypesModel> typessFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<TypesModel>.from(str.map((item) {
    return TypesModel.fromJson(item);
  }));
}

String typessToJson(TypesModel typesModel) {
  final dyn = typesModel.toJson();
  return json.encode(dyn);
}
