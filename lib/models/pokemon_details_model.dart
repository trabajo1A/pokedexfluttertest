import 'dart:convert';

import 'package:pokedex_flutter_app/models/habilidades_model.dart';
import 'package:pokedex_flutter_app/models/movements_model.dart';
import 'package:pokedex_flutter_app/models/types_model.dart';
import 'package:pokedex_flutter_app/models/versiones_model.dart';

class PokemonModel {
  int? id;
  String? image;
  List<HabilidadesModel>? habilidades;
  List<VersionModel>? versiones;
  List<MovementsModel>? movimientos;
  int? peso;
  int? altura;
  int? experienciabase;
  String? name;
  // List<TypesModel>? types;

  PokemonModel({
    this.id,
    this.image,
    this.habilidades,
    this.versiones,
    this.movimientos,
    this.peso,
    this.altura,
    this.experienciabase,
    this.name,
    // this.types,
  });

  factory PokemonModel.fromJson(Map<String, dynamic> json) => PokemonModel(
        id: json["id"],
        image: json["sprites"]["other"]["home"]["front_default"],
        habilidades: List.from(json["abilities"].map((x) {
          return HabilidadesModel.fromJson(x);
        }).toList()),
        versiones: List.from(json["game_indices"].map((x) {
          return VersionModel.fromJson(x);
        }).toList()),
        movimientos: List.from(json["moves"].map((x) {
          return MovementsModel.fromJson(x);
        })),
        peso: json["weight"],
        altura: json["height"],
        experienciabase: json["base_experience"],
        name: json["name"],
        // types: List.from(json["types"].map((x) {
        //   return TypesModel.fromJson(x);
        // })),
      );
}

// List<PokemonModel> pokemonsFromJson(String strJson) {
//   final str = json.decode(strJson);
//   return List<PokemonModel>.from(str.map((item) {
//     return PokemonModel.fromJson(item);
//   }));
// }
