import 'dart:convert';

class VersionModel {
  int? gameIndex;
  String? name;

  VersionModel({
    this.gameIndex,
    this.name,
  });

  factory VersionModel.fromJson(Map<String, dynamic> json) => VersionModel(
        gameIndex: json["game_index"],
        name: json["version"]["name"],
      );

  Map<String, dynamic> toJson() => {
        "game_index": gameIndex,
        "version" "name": name,
      };
}

List<VersionModel> versionesFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<VersionModel>.from(str.map((item) {
    return VersionModel.fromJson(item);
  }));
}

String versionesToJson(VersionModel versionModel) {
  final dyn = versionModel.toJson();
  return json.encode(dyn);
}
