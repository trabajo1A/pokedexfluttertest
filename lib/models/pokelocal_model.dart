class PokeFav {
  int? id;
  String? name;
  String? peso;
  String? altura;
  String? exp;

  PokeFav({
    this.id,
    this.name,
    this.peso,
    this.altura,
    this.exp,
  });

  PokeFav.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map["name"];
    peso = map["peso"];
    altura = map["altura"];
    exp = map["exp"];
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "name": name,
      "peso": peso,
      "altura": altura,
      "exp": exp,
    };
  }
}
