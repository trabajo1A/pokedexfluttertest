import 'dart:convert';

class HabilidadesModel {
  String? name;
  int? slot;

  HabilidadesModel({
    this.name,
    this.slot,
  });

  factory HabilidadesModel.fromJson(Map<String, dynamic> json) =>
      HabilidadesModel(
        name: json["ability"]["name"],
        slot: json["slot"],
      );

  Map<String, dynamic> toJson() => {
        "ability" "name": name,
        "slot": slot,
      };
}

List<HabilidadesModel> habilidadesFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<HabilidadesModel>.from(str.map((item) {
    return HabilidadesModel.fromJson(item);
  }));
}

String habilidadesToJson(HabilidadesModel habilidadesModel) {
  final dyn = habilidadesModel.toJson();
  return json.encode(dyn);
}
