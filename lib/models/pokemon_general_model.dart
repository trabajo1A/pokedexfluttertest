import 'dart:convert';

class PokemonGeneralModel {
  String? name;
  String? url;

  PokemonGeneralModel({
    this.name,
    this.url,
  });

  factory PokemonGeneralModel.fromJson(Map<String, dynamic> json) =>
      PokemonGeneralModel(
        name: json["name"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "url": url,
      };
}

List<PokemonGeneralModel> pokemonsGeneralsFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<PokemonGeneralModel>.from(str.map((item) {
    return PokemonGeneralModel.fromJson(item);
  }));
}

String pokemonsGeneralsToJson(PokemonGeneralModel pokemonGeneralModel) {
  final dyn = pokemonGeneralModel.toJson();
  return json.encode(dyn);
}
