import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:pokedex_flutter_app/models/pokemon_general_model.dart';
import 'package:pokedex_flutter_app/pages/pokemon_details_info_page.dart';
import 'package:pokedex_flutter_app/pages/pokemon_favoritos_page.dart';
import 'package:pokedex_flutter_app/providers/pokemon_provider.dart';

List<PokemonGeneralModel> listaPokemons = <PokemonGeneralModel>[];
List<PokemonGeneralModel> busquedaPokemons = <PokemonGeneralModel>[];

class HomePagePrincipal extends StatefulWidget {
  const HomePagePrincipal({Key? key}) : super(key: key);

  @override
  State<HomePagePrincipal> createState() => _HomePagePrincipalState();
}

class _HomePagePrincipalState extends State<HomePagePrincipal> {
  ScrollController? controller;

  getPokemons() async {
    await getPokemonsHome().then((value) {
      if (this.mounted) {
        setState(() {
          listaPokemons.clear();
          busquedaPokemons.clear();
        });
      }
      listaPokemons = value!;
    }).onError((error, stackTrace) {
      log(error.toString());
    });

    if (listaPokemons.isNotEmpty && this.mounted) {
      setState(() {
        busquedaPokemons = listaPokemons;
      });
    }

    log(busquedaPokemons.toString());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPokemons();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(220, 16, 29, 1),
        title: Text(
          "Pokedex",
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20, top: 16),
            child: GestureDetector(
              onTap: () {
                //ir ala pagina de favoritos
                print("favoritos");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => PokemonFavoritosPage()),
                );
              },
              child: Text(
                "Favoritos",
                style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        children: [
          Expanded(
              child: Container(
            child: ListView.builder(
              controller: controller,
              itemCount: busquedaPokemons.length,
              itemBuilder: (context, position) {
                var item = busquedaPokemons[position];

                return cardLauncher(item);
              },
            ),
          )),
        ],
      ),
    );
  }

  Widget cardLauncher(PokemonGeneralModel datos) {
    return GestureDetector(
      onTap: () {
        log(datos.name.toString());
        log(datos.url.toString());
        log(datos.url!.substring(34));
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => PokeInfoDetailsPage(
                      idPoke: datos.url!.substring(34),
                    )));
      },
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
            side: const BorderSide(
                color: Colors.black, width: 1, style: BorderStyle.solid)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Image(
                  // ignore: prefer_const_constructors
                  image: AssetImage("assets/pokeball.png"),
                  width: MediaQuery.of(context).size.width * .25,
                  fit: BoxFit.fitWidth,
                  loadingBuilder: (context, child, loadingProgress) {
                    if (loadingProgress == null) return child;
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text(
                      datos.name.toString().toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
