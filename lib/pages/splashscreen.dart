import 'dart:async';

import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_flutter_app/main.dart';
import 'package:pokedex_flutter_app/pages/pokemon_favoritos_page.dart';
import 'package:pokedex_flutter_app/pages/pokemon_home_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key, this.route}) : super(key: key);
  final String? route;
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(Duration(milliseconds: 500), () => changeView());
    super.initState();
    print("${routeGlobal} =========>");
  }

  changeView() async {
    Timer(Duration(milliseconds: 1500), () async {
      if (connected1 == false) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => PokemonFavoritosPage()));
      } else {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => HomePagePrincipal()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        child: Column(
          children: [
            Image.asset(
              'assets/pokeball.png',
              fit: BoxFit.fitWidth,
            ),
            Text(
              "Pokedex",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
            ),
          ],
        ),
      ),
    );
  }
}
