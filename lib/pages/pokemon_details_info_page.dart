import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pokedex_flutter_app/database/database.dart';
import 'package:pokedex_flutter_app/models/pokelocal_model.dart';
import 'package:pokedex_flutter_app/models/pokemon_details_model.dart';
import 'package:pokedex_flutter_app/providers/pokemon_provider.dart';
import 'package:pokedex_flutter_app/widgets/loaders.dart';

class PokeInfoDetailsPage extends StatefulWidget {
  const PokeInfoDetailsPage({Key? key, this.idPoke}) : super(key: key);
  final String? idPoke;

  @override
  State<PokeInfoDetailsPage> createState() => _PokeInfoDetailsPageState();
}

class _PokeInfoDetailsPageState extends State<PokeInfoDetailsPage> {
  PokemonModel? pokemon;
  bool existendatos = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getInfoPoke();
  }

  getInfoPoke() async {
    await getDetailsPoke(widget.idPoke.toString()).then((value) {
      print(value!.image.toString());
      print(value.habilidades);
      print(value.altura);
      setState(() {
        pokemon = value;
      });

      if (pokemon != null) {
        setState(() {
          existendatos = true;
        });
      }

      print(pokemon);
    });
  }

  final double circleRadius = 120.0;
  ScrollController? controller;
  ScrollController? controller2;
  ScrollController? controller3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: existendatos == true
            ? cardCentral()
            : Center(child: CircularProgressIndicator()));
  }

  Widget cardCentral() {
    return Container(
      height: double.infinity,
      width: double.infinity,
      color: Color(0xffE0E0E0),
      child: Center(
        child: Stack(children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Stack(
              alignment: Alignment.topCenter,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: circleRadius / 2.0,
                  ),

                  ///here we create space for the circle avatar to get ut of the box
                  child: Container(
                    height: 275.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black26,
                          blurRadius: 8.0,
                          offset: Offset(0.0, 5.0),
                        ),
                      ],
                    ),
                    width: double.infinity,
                    child: Padding(
                        padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                        child: Column(
                          children: <Widget>[
                            SizedBox(
                              height: circleRadius / 2,
                            ),
                            Text(
                              pokemon!.name.toString().toUpperCase(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 34.0),
                            ),
                            Text(
                              pokemon!.id.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0,
                                  color: Colors.lightBlueAccent),
                            ),
                            GestureDetector(
                              onTap: () async {
                                BuildContext circular = context;
                                loadercocacola(circular);
                                log("Toco el corazon");
                                log(pokemon!.altura.toString());
                                log(pokemon!.id.toString());
                                log(pokemon!.experienciabase.toString());
                                log(pokemon!.name.toString());
                                log(pokemon!.peso.toString());
                                final fido = PokeFav(
                                  id: pokemon!.id,
                                  altura: pokemon!.altura.toString(),
                                  exp: pokemon!.experienciabase.toString(),
                                  name: pokemon!.name.toString(),
                                  peso: pokemon!.peso.toString(),
                                );

                                DataBasePoke.db
                                    .validarExistenciaPokemon(pokemon!.id!)
                                    .then((value) {
                                  if (value.isNotEmpty) {
                                    Navigator.pop(circular);
                                    Fluttertoast.showToast(
                                      msg:
                                          "Este pokemon ya forma parte de tu lista de favoritos",
                                      timeInSecForIosWeb: 2,
                                      toastLength: Toast.LENGTH_LONG,
                                      gravity: ToastGravity.TOP,
                                      webShowClose: true,
                                    );
                                  } else {
                                    Timer(Duration(seconds: 5), () {
                                      DataBasePoke.db
                                          .insertReasonCancelled(fido);
                                      Navigator.pop(circular);
                                      Fluttertoast.showToast(
                                        msg: "Agregado a favoritos",
                                        timeInSecForIosWeb: 2,
                                        toastLength: Toast.LENGTH_LONG,
                                        gravity: ToastGravity.TOP,
                                        webShowClose: true,
                                      );
                                    });
                                  }
                                });
                              },
                              child: Icon(
                                Icons.favorite,
                                size: 50,
                                color: Colors.red,
                              ),
                            ),
                            const SizedBox(
                              height: 10.0,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 32.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        'Peso',
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.black54,
                                        ),
                                      ),
                                      Text(
                                        pokemon!.peso.toString(),
                                        style: TextStyle(
                                            fontSize: 34.0,
                                            color: Colors.black87,
                                            fontFamily: ''),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        'Altura',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black54),
                                      ),
                                      Text(
                                        pokemon!.altura.toString(),
                                        style: TextStyle(
                                            fontSize: 34.0,
                                            color: Colors.black87,
                                            fontFamily: ''),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Text(
                                        'EXP',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: Colors.black54),
                                      ),
                                      Text(
                                        pokemon!.experienciabase.toString(),
                                        style: TextStyle(
                                            fontSize: 34.0,
                                            color: Colors.black87,
                                            fontFamily: ''),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            // SizedBox(
                            //   height: 20,
                            // ),
                            // Text(
                            //   'Habilidades',
                            //   style: TextStyle(
                            //     fontSize: 20.0,
                            //     color: Colors.black54,
                            //   ),
                            // ),
                            //listado
                            // Container(
                            //   child: ListView.builder(
                            //     controller: controller,
                            //     itemCount: pokemon!.habilidades!.length,
                            //     itemBuilder: (context, position) {
                            //       var item = pokemon!.habilidades![position];
                            //       return Text(item.name.toString());
                            //     },
                            //   ),
                            // )
                          ],
                        )),
                  ),
                ),

                ///Image Avatar
                Container(
                  width: circleRadius,
                  height: circleRadius,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 8.0,
                        offset: Offset(0.0, 5.0),
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Center(
                      child: Container(
                        child: Image.network(pokemon!.image.toString()),

                        /// replace your image with the Icon
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: circleRadius / .33,
                left: MediaQuery.of(context).size.height * 0.025),
            child: Text(
              'Habilidades',
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.black54,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: circleRadius / .32,
                left: MediaQuery.of(context).size.height * 0.025),
            child: Container(
              height: 100,
              width: 75,
              child: ListView.builder(
                controller: controller,
                itemCount: pokemon!.habilidades!.length,
                itemBuilder: (context, position) {
                  var item = pokemon!.habilidades![position];

                  return Text(item.name.toString());
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: circleRadius / .33,
                left: MediaQuery.of(context).size.height * 0.229),
            child: Text(
              'Versiones',
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.black54,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: circleRadius / .32,
                left: MediaQuery.of(context).size.height * 0.244),
            child: Container(
              height: 150,
              width: 70,
              child: ListView.builder(
                controller: controller2,
                itemCount: pokemon!.versiones!.length,
                itemBuilder: (context, position) {
                  var item = pokemon!.versiones![position];

                  return Text(item.name.toString());
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: circleRadius / .33,
                left: MediaQuery.of(context).size.height * 0.410),
            child: Text(
              'Movimientos',
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.black54,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: circleRadius / .32,
                left: MediaQuery.of(context).size.height * 0.444),
            child: Container(
              height: 150,
              width: 70,
              child: ListView.builder(
                controller: controller3,
                itemCount: pokemon!.movimientos!.length,
                itemBuilder: (context, position) {
                  var item = pokemon!.movimientos![position];

                  return Text(item.name.toString());
                },
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
