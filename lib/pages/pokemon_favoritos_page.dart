import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:pokedex_flutter_app/database/database.dart';
import 'package:pokedex_flutter_app/main.dart';
import 'package:pokedex_flutter_app/models/pokelocal_model.dart';
import 'package:pokedex_flutter_app/widgets/lostconexion.dart';

class PokemonFavoritosPage extends StatefulWidget {
  const PokemonFavoritosPage({Key? key}) : super(key: key);

  @override
  State<PokemonFavoritosPage> createState() => _PokemonFavoritosPageState();
}

class _PokemonFavoritosPageState extends State<PokemonFavoritosPage> {
  List<PokeFav> offPokeFav = <PokeFav>[];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getListFavoritos();
    if (connected1 == false) {
      Timer(Duration(milliseconds: 500), () async {
        alertLostConexion(context);
      });
    }
  }

  getListFavoritos() async {
    var data = await DataBasePoke.db.getAllTasksPokeFav();
    setState(() {
      offPokeFav = data;
    });

    print(offPokeFav);
  }

  ScrollController? controller;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(220, 16, 29, 1),
        title: Text(
          "Mis favoritos",
        ),
        centerTitle: true,
        elevation: 0,
      ),
      body: Column(
        children: [
          offPokeFav.isNotEmpty
              ? Expanded(
                  child: Container(
                  child: ListView.builder(
                    controller: controller,
                    itemCount: offPokeFav.length,
                    itemBuilder: (context, position) {
                      var item = offPokeFav[position];

                      return cardLauncher(item);
                    },
                  ),
                ))
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Image.asset("assets/trainer.png",
                            height: MediaQuery.of(context).size.height * 0.6),
                      ),
                    ),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 50.0, right: 50, top: 15),
                        child: Text(
                          "No tienes pokemons favoritos aún, los que guardes te apareceran aquí",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: 20),
                        ),
                      ),
                    )
                  ],
                ),
        ],
      ),
    );
  }

  Widget cardLauncher(PokeFav datos) {
    return GestureDetector(
      onTap: () {
        log(datos.name.toString());
        log(datos.altura.toString());
      },
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
            side: const BorderSide(
                color: Colors.black, width: 1, style: BorderStyle.solid)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: Image(
                  // ignore: prefer_const_constructors
                  image: AssetImage("assets/poke.png"),
                  width: MediaQuery.of(context).size.width * .25,
                  fit: BoxFit.fitWidth,
                  loadingBuilder: (context, child, loadingProgress) {
                    if (loadingProgress == null) return child;
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  },
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text(
                      datos.name.toString().toUpperCase(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 22,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text(
                      "Altura: ${datos.altura.toString().toUpperCase()}",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text(
                      "Experiencia: ${datos.exp.toString().toUpperCase()}",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 8.0, bottom: 8),
                    child: Text(
                      "Peso: ${datos.peso.toString().toUpperCase()}",
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 15,
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(right: 40.0),
                child: GestureDetector(
                  onTap: () async {
                    log("eliminar");
                    await DataBasePoke.db.deleteDog(datos.id!);
                    getListFavoritos();
                  },
                  child: Icon(
                    Icons.delete,
                    size: 30,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
