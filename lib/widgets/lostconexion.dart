import 'dart:io';

import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';

class LostConexion extends StatefulWidget {
  const LostConexion({Key? key}) : super(key: key);

  @override
  _LostConexionState createState() => _LostConexionState();
}

class _LostConexionState extends State<LostConexion> {
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Text(
              "No tienes conexión a internet\n\nPero puedes ver tus pokemons guardados en favoritos!!!",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 18,
                fontStyle: FontStyle.normal,
                color: Colors.black,
              ),
            ),
          ),
          Lottie.asset('assets/conexion.json'),
          _buildLoginBtn(),
        ],
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.only(left: 30.0, right: 30.0, bottom: 12.0),
      width: double.infinity,
      // ignore: deprecated_member_use
      child: RaisedButton(
        //elevation: 5.0,
        onPressed: () {
          Navigator.pop(context);
        },
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Color.fromRGBO(220, 16, 29, 1),
        child: GestureDetector(
          onTap: null,
          //_launchURL(),
          child: Text(
            'OK',
            style: TextStyle(
              color: Colors.white,
              letterSpacing: 1.5,
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'OpenSans',
            ),
          ),
        ),
      ),
    );
  }
}

alertLostConexion(BuildContext context) {
  AwesomeDialog(
    dismissOnBackKeyPress: false,
    dismissOnTouchOutside: false,
    isDense: false,
    context: context,
    dialogType: DialogType.NO_HEADER,
    animType: AnimType.BOTTOMSLIDE,
    body: LostConexion(),
  )..show();
}
