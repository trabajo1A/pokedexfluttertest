import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:pokedex_flutter_app/models/habilidades_model.dart';
import 'package:pokedex_flutter_app/models/movements_model.dart';
import 'package:pokedex_flutter_app/models/pokemon_details_model.dart';
import 'package:pokedex_flutter_app/models/pokemon_general_model.dart';
import 'package:pokedex_flutter_app/models/types_model.dart';
import 'package:pokedex_flutter_app/models/versiones_model.dart';
import 'package:pokedex_flutter_app/variablesGlobales/variables_globales.dart';

//metodo del home para la lista general de pokemons
Future<List<PokemonGeneralModel>?> getPokemonsHome() async {
  try {
    final queryParameters = {
      'limit': '100',
      'offset': '100',
    };
    final response = await http.get(
      Uri.https("$URL_BASE", "/api/v2/pokemon", queryParameters),
      headers: {"Accept": "aplication.json"},
    );

    var a = json.decode(response.body);
    return pokemonsGeneralsFromJson(json.encode(a["results"]));
    // return pokemonsFromJson(json.decode(a));
  } catch (e) {
    print("NUEVO ERROR : ${e}");
    log(e.toString());
  }
}

//metodo para el detalle de los pokemons debe ser por modelo

Future<PokemonModel?> getDetailsPoke(String idPoke) async {
  try {
    print("=====> ${URL_BASE + "/api/v2/pokemon/" + idPoke}  <=====");
    final response = await http.get(
      Uri.https("$URL_BASE", "/api/v2/pokemon/$idPoke"),
      headers: {"Accept": "aplication.json"},
    );

    var a = json.decode(response.body);

    PokemonModel pokeDetailvar = PokemonModel(
      id: a["id"],
      habilidades: List.from(a["abilities"].map((x) {
        return HabilidadesModel.fromJson(x);
      }).toList()),
      image: a["sprites"]["other"]["home"]["front_default"],
      movimientos: List.from(a["moves"].map((x) {
        return MovementsModel.fromJson(x);
      })),
      peso: a["weight"],
      altura: a["height"],
      name: a["name"],
      experienciabase: a["base_experience"],
      versiones: List.from(a["game_indices"].map((x) {
        return VersionModel.fromJson(x);
      }).toList()),
      // types: a["types"].isNotEmpty()
      //     ? List.from(a["types"].map((x) {
      //         return TypesModel.fromJson(x);
      //       }))
      //     : [],
    );

    return pokeDetailvar;
  } catch (e) {
    print("NUEVO ERROR : ${e}");
    log(e.toString());
  }
}
