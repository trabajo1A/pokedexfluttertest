import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:pokedex_flutter_app/models/pokelocal_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DataBasePoke {
  Database? dataBase;
  String basededatos = "poke.db";
  DataBasePoke._();
  static final DataBasePoke db = DataBasePoke._();

  Future<Database> get database async {
    if (dataBase != null) {
      return dataBase!;
    }
    dataBase = await getDatabaseInstanace();
    return dataBase!;
  }

  Future<Database> getDatabaseInstanace() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(
        directory.path, basededatos); // le asignamos nombre ala base de datos

    return await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      //aqui dentro va la tabla
      //crea el modelo para pasarselo por parametro al insert y delete
      await db.execute("CREATE TABLE " +
          "PokesFavoritos" +
          " (" //ejecutamos el query para crear la tabla con sus atributos
              "id integer,"
              "name TEXT,"
              "peso TEXT,"
              "altura TEXT,"
              "exp TEXT"
              ")");
    });
  }

  // Future<void> insertPokesFav(PokeFav pokeFav) async {
  //   Database? db = await dataBase;
  //   db!.insert("PokesFavoritos", pokeFav.toMap());
  // }

  Future<void> insertReasonCancelled(PokeFav pokeFav) async {
    Database? db = await database;

    db.insert("PokesFavoritos", pokeFav.toMap());
  }

  Future<List<PokeFav>> getAllTasksPokeFav() async {
    Database? db = await database;
    List<Map<String, dynamic>> results = await db.query("PokesFavoritos");
    print(results);
    return results.map((map) => PokeFav.fromMap(map)).toList();
  }

  Future<void> deleteDog(int id) async {
    final db = await database;
    await db.delete(
      'PokesFavoritos',
      where: "id = ?",
      whereArgs: [id],
    );
  }

  Future<List<PokeFav>> validarExistenciaPokemon(int idPoke) async {
    Database? db = await database;
    List<Map<String, dynamic>> result = await db.query(
      "PokesFavoritos",
      where: "id = ?",
      whereArgs: [idPoke],
    );
    print(result);
    return result.map((map) => PokeFav.fromMap(map)).toList();
  }
}
